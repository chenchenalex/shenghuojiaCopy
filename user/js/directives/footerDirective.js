define([], function() {
    var FooterDirective = function($auth) {
    	return {
    		restrict: 'EA',
    	    templateUrl: 'html/templates/footer.html',
    	   	transclude: true,
    	   	replace: true,
    	   	scope: {
                
            },
    	   	link: function($scope, element, attrs) {
                
    	   	}
    	}
    };

    return FooterDirective;
});