define([], function() {
    var MoreInfoDirective = function($auth) {
    	return {
    		restrict: 'EA',
    	    templateUrl: 'html/templates/moreInfo.html',
    	   	transclude: true,
    	   	replace: true,
    	   	scope: {
                
            },
    	   	link: function($scope, element, attrs) {
                
    	   	}
    	}
    };

    return MoreInfoDirective;
});