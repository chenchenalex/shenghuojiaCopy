define([], function() {
    var rankModel = ['$http', function($http) {
        this.getRankList = function(city, month) {
            var data = $http.post('../api/web/?r=rank/city', {
                    city: city,
                    month: month
                })
                .then(function(response) {
                    return response.data;
                });
            return data;
        }

    }];

    return rankModel;
});
