define([
	'config/UrlConfig',

	'controllers/meController',
	'controllers/rankController',
	'controllers/loginController',

	'directives/footerDirective',
	'directives/moreInfoDirective',

	'services/authService',
	'analyticsService',

	'model/rankModel',

	'ui.router',
	'ui.bootstrap'
], function(
	UrlConfig,

	MeController,
	RankController,
	LoginController,

	FooterDirective,
	MoreInfoDirective,

	AuthService,
	AnalyticsService,

	RankModel
){
	var app = angular.module('app', ['ui.router', 'ui.bootstrap']);

	app.config(UrlConfig);

	app.controller('MeController', MeController);
	app.controller('RankController', RankController);
	app.controller('LoginController', LoginController);

	app.directive('footer', FooterDirective);
	app.directive('moreInfo', MoreInfoDirective);

	app.service('$auth', AuthService);
	app.service('$analytics', AnalyticsService);
	app.service('RankModel', RankModel);

	// Add Analytics before running
	app.run(function($rootScope, $location, $analytics) {
		$rootScope.$on('$stateChangeStart', function() {
			$analytics.recordPageview($location.absUrl());
		});
	});
});
