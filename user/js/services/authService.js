define([], function() {
	var $auth = function($http) {
		this.getLogin = function(config) {
			if (!config) {
				config = {
					'redirectWhenFailed': true
				};
			};

			var data = $http.get('../api/web/?r=user/login').then(function(res) {
				if (res.data.status.code == 1) {
					return res.data;
				} else {
					if (config.redirectWhenFailed) {
						location.href = '#login';
						return {admin: {}};
					} else {
						return null;
					}
				}
			});
			
			return data;
		};

		this.postLogin = function(tel) {
			var data = $http({
				method: 'post',
				url: '../api/web/?r=user/login',
				data: {
					'tel': tel
				}
			}).then(function(res) {
				return res.data;
			});
			
			return data;
		};

		this.logout = function() {
			$http.get('../api/web/?r=user/logout').then(function() {
				location.href = '#/login';
			});
		};
	};

    return $auth;
});
