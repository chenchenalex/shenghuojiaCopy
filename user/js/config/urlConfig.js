define([],function(){
	var UrlConfig = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/me');
        
        $stateProvider
            .state('me', {
                url: '/me',
                templateUrl: 'html/me.html',
                controller: 'MeController'
            })
            .state('rank', {
                url: '/rank',
                templateUrl: 'html/rank.html',
                controller: 'RankController'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'html/login.html',
                controller: 'LoginController'
            })
    }]; 
		
    return UrlConfig;
})