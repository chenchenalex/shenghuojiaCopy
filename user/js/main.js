require.config({
    baseUrl: 'js',
    paths: {
        'angular': '../../common/js/lib/angular.min',
        'ui.bootstrap': '../../common/js/lib/ui-bootstrap-tpls.min',
        'ui.router': '../../common/js/lib/angular-ui-router.min',
        'underscore': '../../common/js/lib/underscore.min',
        'ga': '../../common/js/lib/google-analytics',
        'analyticsService': '../../common/js/services/analyticsService'
    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        'underscore': {
            exports: '_'
        },
        'ui.bootstrap': ['angular'],
        'ui.router': ['angular']
    }
});

require(['app-module'], function() {
    angular.bootstrap(document, ['app']);
});
