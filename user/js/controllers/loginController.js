define([], function() {
    var LoginController = ['$scope', '$http', '$auth', function($scope, $http, $auth) {
    	$scope.tryLogin = function() {
    		$auth.postLogin($scope.tel)
	    		.then(function(data) {
		            if (data.status.code == 1) {
		            	location.href = '#/overview';
		            } else {
		            	alert('小美没有在系统里找到你的手机号哦，请重试~');
		            }
		        });
    	};
    }];

    return LoginController;
});