define([], function() {
    var MeController = ['$scope', '$http', '$auth', function($scope, $http, $auth) {

        /** 
         * Init 
         */

        $scope.monthList = _.range(1, 13);
        $scope.currentMonth = ((new Date()).getMonth() + 1).toString();

        /** 
         * 登录
         */

        $auth.getLogin().then(function(data) {
            if (data.user) {
                $scope.user = data.user;
                refresh(data.user.id, $scope.currentMonth);
            }
        });

        /** 
         * 修改月份
         */

        $scope.changeMonth = function(month) {
            refresh($scope.user.id, month);
        };

        /** 
         * 刷新整个页面
         */

        function refresh(id, month) {
            $http.post('../api/web/?r=user/overview', {
                'id': id,
                'month': month
            }).then(function(response) {
                $scope.overview = response.data;
                $scope.overview.check.allCount 
                    = $scope.overview.check.unchecked.length 
                    + $scope.overview.check.approved.length 
                    + $scope.overview.check.disapproved.length;
            });
        }

        /** 
         * 登出
         */

        $scope.logout = function() {
            $auth.logout();
        };
    }];

    return MeController;
});