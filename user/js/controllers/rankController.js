define(['underscore'], function() {
    var RankController = ['$scope', '$http', '$auth', 'RankModel', function($scope, $http, $auth, rankModel) {
        /* Init */
        $scope.cityTabList = ['全国', '北京', '上海', '广州', '长沙'];
        var currentCity = $scope.cityTabList[0];
        $scope.currentMonth = ((new Date()).getMonth() + 1).toString();
        $scope.radio = {};
        $scope.rankList = {};
        /* Login */
        $auth.getLogin({
            'redirectWhenFailed': false
        }).then(function(data) {
            $scope.radio.city = $scope.cityTabList[0];
            $scope.getRankList($scope.currentMonth);
        });

        /* Get city list */
        $scope.getRankList = function(month) {
            rankModel.getRankList($scope.cityTabList[0], month).then(function successs(response) {
                $scope.rankList = response;
                $scope.updateCityList(currentCity);
            });
        };

        /*Update city list*/
        $scope.updateCityList = function(city) {
                currentCity = city;
                if (city == $scope.cityTabList[0]) {
                    $scope.currentCityList = $scope.rankList;
                    return;
                }
                $scope.currentCityList = _.where($scope.rankList, {"city": city});

            }


        /* Month selection */
        $scope.monthList = _.range(1, 13);
    }];

    return RankController;
});
