define(['ga'], function() {
	var $analytics = function() {
		this.recordPageview = function(url) {
		    ga('send', 'pageview', url);
		};
	};

    return $analytics;
});