define([], function() {
	var $auth = ['$http', function($http) {
		this.getLogin = function() {
			var data = $http.get('../api/web/?r=admin/login').then(function(res) {
				if (res.data.status.code) {
					return res.data;
				} else {
					location.href = '#login';
					return {admin: {}};
				}
			});
			
			return data;
		};

		this.postLogin = function(id, pass) {
			var data = $http({
				method: 'post',
				url: '../api/web/?r=admin/login',
				data: {
					'id': id, 
					'pass': pass
				}
			}).then(function(res) {
				return res.data;
			});
			
			return data;
		};

		this.logout = function() {
			$http.get('../api/web/?r=admin/logout').then(function() {
				location.href = '#/login';
			});
		};
		
		this.resetPass = function(adminid, oldPass, newPass) {
			$http({
				method: 'post',
				url: '../api/web/?r=admin/reset-pass',
				data: {
					adminid: adminid, 
					oldPass: oldPass,
					newPass: newPass
				}
			}).then(function(res) {
				if (res.data.status == true) {
					location.href = '#/login';
				}
			});
		};
	}];

    return $auth;
});
