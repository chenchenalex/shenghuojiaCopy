define([], function() {
    var HeaderDirective = function($auth) {
    	return {
    		restrict: 'EA',
    	    templateUrl: 'html/templates/header.html',
    	   	transclude: true,
    	   	replace: true,
    	   	scope: {
                adminName: '=adminName'
            },
    	   	link: function($scope, element, attrs) {
                $scope.logout = function() {
                    $auth.logout();
                };
    	   	}
    	}
    };

    return HeaderDirective;
});