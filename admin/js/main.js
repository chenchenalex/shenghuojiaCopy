require.config({
    baseUrl: 'js',
    paths: {
        'angular': '../../common/js/lib/angular.min',
        'angular-bootstrap': '../../common/js/lib/ui-bootstrap-tpls.min',
        'angular-router': '../../common/js/lib/angular-ui-router.min',
        'angular-chart': '../../common/js/lib/angular-chart.min',
        'chart': '../../common/js/lib/chart.min',
        'underscore': '../../common/js/lib/underscore.min',
        'ga': '../../common/js/lib/google-analytics',
        'analyticsService': '../../common/js/services/analyticsService'
    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        'underscore': {
            exports: '_'
        },
        'angular-bootstrap': ['angular'],
        'angular-router': ['angular'],
        'angular-chart': ['angular', 'chart']
    }
});

require(['app-module'], function() {
    angular.bootstrap(document, ['app']);
});