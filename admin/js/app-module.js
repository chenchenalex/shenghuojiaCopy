define([
	'config/UrlConfig',

	'controllers/checkController',
	'controllers/checkModalController',
	'controllers/PointController',
	'controllers/PointModalController',
	'controllers/LoginController',
	'controllers/UserListController',
	'controllers/UserDetailController',
	'controllers/ImportController',
	'controllers/SettingsController',
	'controllers/LinkController',
	'controllers/StatController',

	'models/CheckModel',
	'models/PointModel',
	'models/LinkModel',
	'models/StatModel',
	
	'directives/HeaderDirective',

	'services/AuthService',
	'analyticsService',

	'angular-router',
	'angular-bootstrap',
	'angular-chart'
], function(
	UrlConfig,

	CheckController,
	CheckModalController,
	PointController,
	PointModalController,
	LoginController,
	UserListController,
	UserDetailController,
	ImportController,
	SettingsController,
	LinkController,
	StatController,

	CheckModel,
	PointModel,
	LinkModel,
	StatModel,
	
	HeaderDirective,

	AuthService,
	AnalyticsService
){
	var app = angular.module('app', ['ui.router', 'ui.bootstrap', 'chart.js']);

	/************************************
	 *                                  *
	 *              Config              *
	 *                                  *
	 ************************************/

	/**
	 * Router config
	 */

	app.config(UrlConfig);

	/************************************
	 *                                  *
	 *           Controllers            *
	 *                                  *
	 ************************************/

	app.controller('CheckController', CheckController);
	app.controller('CheckModalController', CheckModalController);
	app.controller('PointController', PointController);
	app.controller('PointModalController', PointModalController);
	app.controller('LoginController', LoginController);
	app.controller('UserListController', UserListController);
	app.controller('UserDetailController', UserDetailController);
	app.controller('ImportController', ImportController);
	app.controller('SettingsController', SettingsController);
	app.controller('LinkController', LinkController);
	app.controller('StatController', StatController);

	/************************************
	 *                                  *
	 *              Models              *
	 *                                  *
	 ************************************/

	app.service('CheckModel', CheckModel);
	app.service('PointModel', PointModel);
	app.service('LinkModel', LinkModel);
	app.service('StatModel', StatModel);

	/************************************
	 *                                  *
	 *            Directives            *
	 *                                  *
	 ************************************/

	app.directive('header', HeaderDirective);

	/************************************
	 *                                  *
	 *             Services             *
	 *                                  *
	 ************************************/

	app.service('$auth', AuthService);
	app.service('$analytics', AnalyticsService);

	/************************************
	 *                                  *
	 *            Run blocks            *
	 *                                  *
	 ************************************/

	/**
	 * Google Analytics
	 */

	app.run(function($rootScope, $location, $analytics) {
		$rootScope.$on('$stateChangeStart', function() {
			$analytics.recordPageview($location.absUrl());
		});
	});
});
