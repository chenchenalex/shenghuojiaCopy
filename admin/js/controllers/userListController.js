define([], function() {
    var UserListController = ['$scope', '$http', '$auth', '$filter', function($scope, $http, $auth, $filter) {
    	/* Init */
    	var data = [];

    	/* Login */
    	$auth.getLogin().then(function(data) {
    	    $scope.admin = data.admin;
    	});

    	/* Pagination config */
        $scope.currentPage = 1;
        $scope.maxPerPage = 10;

        /* Get user list */
        $http.get('../api/web/?r=user/list').then(function success(response) {
            $scope.data = response.data;
            $scope.filtedList = $scope.data;

            $scope.cityList = [];
            for (var i = 0; i < $scope.data.length; i++) {
                if ($scope.cityList.indexOf($scope.data[i].city) == -1) {
                    $scope.cityList.push($scope.data[i].city);
                }
            }
        });

        /* Filter */
        $scope.$watchCollection('search', function(term) {
            $scope.filtedList = $filter('filter')($scope.data, term);
        });
    }]; 

    return UserListController;
});