define([], function() {
    var SettingsController = function($scope, $auth) {
    	/* Login */
        $auth.getLogin().then(function(data) {
            $scope.admin = data.admin;
        });

        /* Reset Pass */
        $scope.tryReset = function() {
            $auth.resetPass($scope.admin.id, $scope.admin.oldPass, $scope.admin.newPass);
        };
    };

    return SettingsController;
})