/**
 * CheckController
 *
 * - Url: #/check
 * - View: html/check.html
 * - Model: js/models/CheckModel.js
 */

 define([], function() {
    var CheckController = ['CheckModel', '$scope', '$modal', '$http', '$auth', '$filter', 
        function(CheckModel, $scope, $modal, $http, $auth, $filter) {

            /************************************
             *                                  *
             *               Init               *
             *                                  *
             ************************************/

            /**
             * Try login
             *
             * 1. If login is approved, then get admin info.
             * 2. If login is disapproved, then redirect url to #/login.
             */

            $auth.getLogin().then(function(data) {
                $scope.admin = data.admin;
            });


            /**
             * Refresh
             *
             * Get check list, and then
             * 1. Init city list on the filter section
             * 2. Init filted list before filter starts watching
             */

            $scope.refresh = function(options) {
                CheckModel.getList().then(function(data) {
                    $scope.checkListData = data;
                    if (options) {
                        switch(options.type) {
                            case 'init':
                                // Init filter section
                                $scope.cityList = selectDistinct("city", data);
                                $scope.executorList = selectDistinct("executor", data);
                                $scope.sourceList = selectDistinct('source', data);
                                $scope.statusList = CheckModel.getStatusTypes();

                                // Init filted list before watching
                                $scope.filtedList = data;

                                break;
                        }
                    }
                });
            };

            $scope.refresh({type: 'init'});

            /************************************
             *                                  *
             *              Config              *
             *                                  *
             ************************************/

            /**
             * Init config
             */

            $scope.config = {};

            /**
             * Load config of pagination
             */

            $scope.config.pagination = {
                'currentPage': '1',
                'maxPerPage': '10'
            };

            /**
             * Load config of score range
             */

            $scope.config.scoreRange = {
                'begin': 0,
                'end': 100
            };

            /**
             * Load config of order
             */

            $scope.config.order = {
                predicate: [],
                reverse: false
            };

            
            /************************************
             *                                  *
             *              Events              *
             *                                  *
             ************************************/

            /**
             * Open modal
             * for users to edit notes.
             */

            $scope.openModal = function(commentid, notes) {
                var modalInstance = $modal.open({
                    animation: true,
                    templateUrl: 'html/checkModal.html',
                    controller: 'CheckModalController',
                    size: 'lg',
                    resolve: {
                        commentid: function() { return commentid; },
                        notes: function() { return notes; },
                        executor: function() { return $scope.admin.name; }
                    }
                });

                modalInstance.result.then(function () {
                    $scope.refresh();
                });
            };

            /**
             * Update score
             */

            $scope.updateScore = function(commentid, newScore) {
                // Post data to the server
                $http.post('../api/web/?r=check/score', {
                        'commentid': commentid,
                        'score': newScore,
                        'executor': $scope.admin.name
                    }).
                    then(function success(response) {
                        alert('修改成功！');
                        $scope.refresh();
                    });
            };

            /**
             * Update check status
             * after clicking the radio button in the check status column. 
             */

            $scope.updateStatus = function(commentid, newStatus, notes) {

                // Post data to the server
                $http.post('../api/web/?r=check/update', {
                        'commentid': commentid,
                        'checkStatus': newStatus,
                        'executor': $scope.admin.name
                    }).
                    then(function success(response) {

                        $scope.refresh();
                        
                        // If the new status is "Disapproved", 
                        // then open the modal for the user to write notes.
                        if(newStatus == '2') {
                            $scope.openModal(commentid, notes);
                        } 
                    });
            };

            /**
             * Anti cheating button
             * 
             * It will search the comment content in Baidu to find the similar content.
             */

            $scope.antiCheating = function(content) {
                // Edit the string to be searched, or the search may fail.
                var contentCut = content.substr(0, 35);

                // Open Baidu to search the content.
                window.open('http://www.baidu.com/s?wd=' + contentCut);
            };

            /************************************
             *                                  *
             *              Watchers            *
             *                                  *
             ************************************/

            /**
             * Enable filter to work with pagination
             */

            $scope.$watchCollection('search', function(newValue) {
                $scope.filtedList = $filter('filter')($scope.checkListData, newValue);
            });

            $scope.$watchCollection('checkListData', function(newValue) {
                $scope.filtedList = $filter('filter')(newValue, $scope.search);
                $scope.filtedList = $filter('orderBy')($scope.filtedList, $scope.config.order.predicate, $scope.config.order.reverse);
            });

            /**
             * Enable filter to work with order
             */

            $scope.$watchCollection('config.order', function(newValue) {
                $scope.filtedList = $filter('orderBy')($scope.filtedList, newValue.predicate, newValue.reverse);
            });

            /************************************
             *                                  *
             *             Functions            *
             *                                  *
             ************************************/

            /**
             * selectDistinct
             *
             * Output the list of the selectDistinct element in the array[key].
             * Used for a 2-dimensional array.
             * 
             * @key (string)
             * @array (array)
             */

            function selectDistinct(key, array) {
                var output = [];
                for (var i = 0; i < array.length; i++) {
                    var value = array[i][key];
                    if (output.indexOf(value) == -1 && value) {
                        output.push(value);
                    }
                }
                return output;
            }

            /**
             * byRange for filter
             */

            // $scope.byRange = function (fieldName, minValue, maxValue) {
            //     if (minValue === undefined) minValue = Number.MIN_VALUE;
            //     if (maxValue === undefined) maxValue = Number.MAX_VALUE;

            //     return function predicateFunc(item) {
            //         return minValue <= item[fieldName] && item[fieldName] <= maxValue;
            //     };
            // };

            /**
             * Order
             */

            $scope.order = function(predicate) {
                if($scope.config.order.predicate[0] === predicate){
                    $scope.config.order.reverse = !$scope.config.order.reverse;
                }
                else{
                    $scope.config.order.reverse = true;
                    $scope.config.order.predicate = [predicate,'id'];
                }
            };
        }
    ];

    return CheckController;
});
