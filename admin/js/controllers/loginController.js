define(['models/adminAvatorsModel'], function(adminAvatorsModel) {
    var loginController = function($scope, $http, $auth) {
        // Default adminid
        $scope.id = 1;

    	$scope.tryLogin = function() {
    		$auth.postLogin($scope.id, $scope.pass)
	    		.then(function(data) {
		            if (data.status.code == 1) {
		            	location.href = '#/check';
		            } else {
		            	console.log('oh no');
		            }
		        });
    	};

    	$scope.quickSelect = function(id) {
    		$scope.id = id;
    	};

    	$scope.avatorList = adminAvatorsModel.get();
    };

    return loginController;
});