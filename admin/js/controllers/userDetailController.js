define([], function() {
    var UserDetailController = ['$scope', '$http', '$auth', '$stateParams', function($scope, $http, $auth, $stateParams) {
    	/* Init */
    	$scope.user = {};
    	var month = (new Date()).getMonth() + 1;

    	/* Login */
    	$auth.getLogin().then(function(data) {
    	    $scope.admin = data.admin;
    	});

    	/* Get userid from url */
    	var id = $stateParams.id;

    	/* Get user info */
    	$http.post('../api/web/?r=user/info', {id: id}).then(function success(response) {
    	    $scope.user.info = response.data;
    	});

    	/* Get user overview */
    	$http.post('../api/web/?r=user/overview', {id: id, month: month}).then(function success(response) {
    	    $scope.user.overview = response.data;
    	    $scope.user.overview.check.allCount = $scope.user.overview.check.approved.length + $scope.user.overview.check.disapproved.length + $scope.user.overview.check.unchecked.length;
    	});

    	/* Get user logs */
    	$http.post('../api/web/?r=log/user&id=', {id: id}).then(function success(response) {
    	    $scope.user.log = response.data;
    	});
    }]; 

    return UserDetailController;
});