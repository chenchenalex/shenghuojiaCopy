/**
 * StatController
 *
 * - Url: #/stat
 * - View: html/stat.html
 */

define(['underscore', 'chart'], function() {
    var StatController = ['StatModel', '$scope', '$http', '$auth', '$timeout',
        function(StatModel, $scope, $http, $auth, $timeout) {

            /************************************
             *                                  *
             *              Config              *
             *                                  *
             ************************************/

            /**
             * Init config
             */

            $scope.config = {
                'mode': 'daily',
                'modeList': ['daily', 'weekly', 'monthly', 'yearly'],
                'fields': StatModel.getFields(),
                'numColumnsShow': 3,
                'numPeriodsShow': 12,
                'begin': false,
                'end': false,
                'calendar': {}
            };

            //Attention, rules in newer versions of angularjs has changed here (Now 1.3.14)
            $scope.monthConfig = {
                'datepicker-mode': "'month'",
                'min-mode': "'month'"
            };

            $scope.yearConfig = {
                'datepicker-mode': "'year'",
                'min-mode': "'year'"
            };

            /**
             * Chart.js
             */

            Chart.defaults.global.scaleBeginAtZero = true;
            Chart.defaults.global.maintainAspectRatio = true;

            /************************************
             *                                  *
             *               Init               *
             *                                  *
             ************************************/

            /**
             * Try login
             */

            $auth.getLogin().then(function(data) {
                $scope.admin = data.admin;
            });

            /**
             * Get daily data
             */

            StatModel.getResult().then(function(data) {
                $scope.data = data;
                setDefaultInterval();
                refreshCharts();
            });

            /************************************
             *                                  *
             *              Events              *
             *                                  *
             ************************************/

            $scope.refreshCharts = function() {
                refreshCharts();
            };

            $scope.openCalendar = function(e, name) {
                e.preventDefault();
                $scope.config[name] = true;
            };


            /************************************
             *                                  *
             *             Functions            *
             *                                  *
             ************************************/

            /**
             * Set default interval
             */

            function setDefaultInterval() {
                var data = $scope.data;
                var modeList = $scope.config.modeList;
                var numPeriodsShow = $scope.config.numPeriodsShow;
                var defaultInterval = {},
                    calenderInterval = {};
                _.each(modeList, function(mode) {
                    var period = data[mode].period;
                    defaultInterval[mode] = {
                        'begin': (period.length > numPeriodsShow ? period[period.length - numPeriodsShow] : _.first(period)),
                        'end': _.last(period)
                    };

                    /*
                     * transform string to date object used in calendar
                     * datepicker in angularjs only accepts date() as parameter of time
                     */

                    switch (mode) {
                        case 'daily':
                            calenderInterval[mode] = {
                                'begin': new Date(parseInt(defaultInterval[mode].begin.slice(0, 4)), parseInt(defaultInterval[mode].begin.slice(4, 6)) - 1, parseInt(defaultInterval[mode].begin.slice(6, defaultInterval[mode].begin.length))),
                                'end': new Date(parseInt(defaultInterval[mode].end.slice(0, 4)), parseInt(defaultInterval[mode].end.slice(4, 6)) - 1, parseInt(defaultInterval[mode].end.slice(6, defaultInterval[mode].end.length)))
                            };
                            break;
                        case 'monthly':
                            calenderInterval[mode] = {
                                'begin': new Date(parseInt(defaultInterval[mode].begin.slice(0, 4)), parseInt(defaultInterval[mode].begin.slice(4, 6)) - 1),
                                'end': new Date(parseInt(defaultInterval[mode].end.slice(0, 4)), parseInt(defaultInterval[mode].end.slice(4, 6)) - 1)
                            };
                            break;
                        case 'yearly':
                            calenderInterval[mode] = {
                                'begin': new Date(parseInt(defaultInterval[mode].begin), 0, 1),
                                'end': new Date(parseInt(defaultInterval[mode].end), 0, 1)
                            };
                            break;
                    }
                });

                $scope.config.interval = defaultInterval;
                $scope.config.calendar = calenderInterval;
            }

            /**
             * Refresh chart
             */

            function refreshCharts() {

                // Get data from $scope

                var srcData = $scope.data;
                var modeList = $scope.config.modeList;
                var interval = $scope.config.interval;
                var fields = $scope.config.fields;

                // Calculate the interval indexes (begin index & end index)
                var intervalIndexes = getIntervalIndexes(srcData, interval);

                // Slice labels and data, for the use of $scope.chartData
                var data = sliceData(srcData, intervalIndexes);

                // Set chart data
                var chartData = {};
                _.each(modeList, function(mode) {
                    chartData[mode] = {};
                    _.each(fields, function(field, index) {
                        var currentCategory = field.categoryName;
                        chartData[mode][currentCategory] = [
                            currentCategory, []
                        ];
                        _.each(field.categoryItems, function(item) {
                            chartData[mode][currentCategory][1].push({
                                'series': item.series,
                                'data': generateDataArray(data, mode, item.dataLabels),
                                'labels': data[mode].period
                            });
                        });
                    });
                });

                // Put data to $scope
                $scope.chartData = chartData;

                // To genetate a data array for chartData
                // Input:  ['xxx', 'yyy', ...]  <-- dataLabels
                // Output: [[1,2,3], [4,5,6], ...]
                function generateDataArray(data, mode, dataLabels) {
                    var arr = [];
                    _.each(dataLabels, function(dataLabel) {
                        arr.push(data[mode][dataLabel]);
                    });
                    return arr;
                }
            }

            /**
             * Get inteval indexes
             */

            function getIntervalIndexes(data, interval) {
                var modeList = $scope.config.modeList;
                var intervalIndexes = {};
                _.each(modeList, function(mode) {
                    if (interval[mode] !== undefined) {
                        intervalIndexes[mode] = {
                            'begin': _.indexOf(data[mode].period, interval[mode].begin),
                            'end': _.indexOf(data[mode].period, interval[mode].end) + 1
                        };
                    }
                });

                return intervalIndexes;
            }

            /**
             * Slice data according to the interval indexes
             *
             * Input:
             * > srcData: {daily: {date: [srcArray], ...}, ...}
             * > intervalIndexs: {daily: {begin: ..., end: ...}, ...}
             *
             * Output:
             * > data: {daily: {date: [slicedArray], ...}, ...}
             */

            function sliceData(srcData, intervalIndexes) {
                var modeList = $scope.config.modeList;
                var data = {};

                _.each(modeList, function(mode) {
                    data[mode] = {};

                    for (var key in srcData[mode]) {
                        switch (mode) {
                            case 'daily':
                                data[mode][key] = srcData[mode][key].slice(
                                    intervalIndexes.daily.begin,
                                    intervalIndexes.daily.end
                                );
                                break;

                            case 'weekly':
                                data[mode][key] = srcData[mode][key].slice(
                                    intervalIndexes.weekly.begin,
                                    intervalIndexes.weekly.end
                                );
                                break;

                            case 'monthly':
                                data[mode][key] = srcData[mode][key].slice(
                                    intervalIndexes.monthly.begin,
                                    intervalIndexes.monthly.end
                                );
                                break;

                            case 'yearly':
                                data[mode][key] = srcData[mode][key].slice(
                                    intervalIndexes.yearly.begin,
                                    intervalIndexes.yearly.end
                                );
                                break;
                        }
                    }
                });

                return data;
            }

            //ensure month/day with 2 digits
            function dateFormat(num) {
                var str = "0" + num;
                return str.substring(str.length - 2, str.length);
            }

            /* transform data into string
             * After users select a new date, calendar will generate
             * a new date called exportDate and this will be transformed
             * to string and passed for diagram uses
             * input: user's input;
             * output: $scope.config.interval['daily']/['monthly']/['yearly']
             */

            $scope.$watch('config.calendar', function(newValue) {
                var exportDate = {
                    daily: {},
                    monthly: {},
                    yearly: {}
                };

                if (newValue.daily !== undefined) {
                    exportDate.daily.begin = newValue.daily.begin.getFullYear().toString() + dateFormat(newValue.daily.begin.getMonth() + 1) + dateFormat(newValue.daily.begin.getDate());
                    exportDate.daily.end = newValue.daily.end.getFullYear().toString() + dateFormat(newValue.daily.end.getMonth() + 1) + dateFormat(newValue.daily.end.getDate());

                    exportDate.monthly.begin = newValue.monthly.begin.getFullYear().toString() + dateFormat(newValue.monthly.begin.getMonth() + 1);
                    exportDate.monthly.end = newValue.monthly.end.getFullYear().toString() + dateFormat(newValue.monthly.end.getMonth() + 1);

                    exportDate.yearly.begin = newValue.yearly.begin.getFullYear().toString();
                    exportDate.yearly.end = newValue.yearly.end.getFullYear().toString();

                    $scope.config.interval.daily = exportDate.daily;
                    $scope.config.interval.monthly = exportDate.monthly;
                    $scope.config.interval.yearly = exportDate.yearly;
                }

                //Wait until data is loaded
                if ($scope.data) {
                    refreshCharts();
                }

            }, true);
        }
    ];

    return StatController;
});
