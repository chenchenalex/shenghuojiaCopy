/**
 * PointModalController
 *
 * - Url: #/point
 * - View: html/pointModal.html
 */

define([], function() {
    var PointModalController = ['$scope', '$modalInstance', '$http', 'filtedList', 'index', 'addOrReduce', 'executor',
        function($scope, $modalInstance, $http, filtedList, index, addOrReduce, executor) {

            /************************************
             *                                  *
             *               Init               *
             *                                  *
             ************************************/

            /**
             * Init original notes
             */

            $scope.notes = '';

            /**
             * Init alerts
             */

            $scope.alerts = [];

            /**
             * Init quick comment options
             */

            $scope.quickCommentOptions = ['奖品兑换', '任务修正'];

            /**
             * Init addOrReduce to be used in modal header
             */

            $scope.addOrReduce = addOrReduce;
            
            /************************************
             *                                  *
             *              Events              *
             *                                  *
             ************************************/

            /**
             * Confirm
             */

            $scope.ok = function() {

                if ($scope.notes) {

                    // Calculate delta
                    var delta = addOrReduce * $scope.delta;

                    // Upload to the server
                    $http.post('../api/web/?r=point/update', {
                            'id': filtedList[index].id,
                            'delta': delta,
                            'notes': $scope.notes,
                            'executor': executor
                        }).
                        then(function success(response) {

                            // Update data in scope after data is uploaded successfully
                            filtedList[index].point = parseInt(filtedList[index].point) + parseInt(delta);
                            filtedList[index].executor = executor;

                            // Close the modal
                            $modalInstance.close($scope.status);
                        });
                        
                } else {

                    // Show alert when notes are empty
                    $scope.alerts.push({
                        type: 'danger',
                        msg: '请填写备注！'
                    });
                }
            };

            /**
             * Close alert box
             */

            $scope.closeAlert = function(index) {
                $scope.alerts.splice(index, 1);
            };

            /**
             * Cancel
             */

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };

            /**
             * Add quick comment
             */

            $scope.addQuickComment = function(text) {
                $scope.notes = $scope.notes + text + '\n';
            };
        }
    ];

    return PointModalController;
})
