/**
 * ImportController
 *
 * - Url: #/import
 * - View: html/import.html
 */

define([], function() {
    var ImportController = ['$scope', '$http', '$auth', function($scope, $http, $auth) {

        // Nothing to do now.
        
    }]; 

    return ImportController;
});