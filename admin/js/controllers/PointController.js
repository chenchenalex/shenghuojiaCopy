/**
 * PointController
 *
 * - Url: #/point
 * - View: html/point.html
 * - Model: js/models/PointModel.js
 */

define([], function() {
    var PointController = ['PointModel', '$scope', '$modal', '$http', '$auth', '$filter', 
        function(PointModel, $scope, $modal, $http, $auth, $filter) {

            /************************************
             *                                  *
             *               Init               *
             *                                  *
             ************************************/

            /**
             * Try login
             *
             * 1. If login is approved, then get admin info.
             * 2. If login is disapproved, then redirect url to #/login.
             */

            $auth.getLogin().then(function(data) {
                $scope.admin = data.admin;
            });

            /**
             * Refresh all when init
             */

            refresh();

            /**
             * Enable filter to work with pagination
             */

            $scope.$watchCollection('search', function(term) {
                $scope.filtedList = $filter('filter')($scope.pointListData, term);
            });
            
            /************************************
             *                                  *
             *              Config              *
             *                                  *
             ************************************/

            /**
             * Init config
             */

            $scope.config = {};

            /**
             * Load pagination config
             */

            $scope.config.pagination = {
                currentPage: 1,
                maxPerPage: '10'
            };

            /************************************
             *                                  *
             *              Events              *
             *                                  *
             ************************************/

            /**
             * Open modal to modify points
             */

            $scope.openModal = function(index, addOrReduce) {
                var modalInstance = $modal.open({
                    animation: true,
                    templateUrl: 'html/pointModal.html',
                    controller: 'PointModalController',
                    size: 'lg',
                    resolve: {
                        filtedList: function() { return $scope.filtedList; },
                        index: function() { return index; },
                        executor: function() { return $scope.admin.name; },
                        addOrReduce: function() { return addOrReduce; }
                    }
                });
            };

            /**
             * Refresh
             *
             * Get point list, and then
             * 1. Init city list on the filter section
             * 2. Init filted list before filter starts watching
             */

            function refresh() {
                PointModel.getList().then(function(data) {
                    $scope.pointListData = data;

                    // Init city list on the filter section
                    $scope.cityList = [];
                    for (var i = 0; i < $scope.pointListData.length; i++) {
                        if ($scope.cityList.indexOf($scope.pointListData[i].city) == -1) {
                            $scope.cityList.push($scope.pointListData[i].city);
                        }
                    }

                    // Init filted list before watching
                    $scope.filtedList = $scope.pointListData;
                });
            }
        }
    ];

    return PointController;
});