/**
 * LinkController
 *
 * - Url: #/link
 * - View: html/link.html
 * - Model: js/models/LinkModel.js
 */

define([], function() {
    var LinkController = ['LinkModel', '$scope', '$http', '$auth',
        function(LinkModel, $scope, $http, $auth) {

            /************************************
             *                                  *
             *               Init               *
             *                                  *
             ************************************/

        	/**
             * Login and get Admin info
             */

        	$auth.getLogin().then(function(data) {
        	    $scope.admin = data.admin;
        	});

        	/**
             * Get Links
             */
        	$scope.links = LinkModel.getLinkList();
        }
    ];

    return LinkController;
});