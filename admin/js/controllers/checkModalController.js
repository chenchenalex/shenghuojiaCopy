/**
 * CheckModalController
 *
 * - Url: #/check
 * - View: html/checkModal.html
 */

 define([], function() {
    var CheckModalController = ['$scope', '$modalInstance', '$http', 'commentid', 'notes', 'executor',
        function($scope, $modalInstance, $http, commentid, notes, executor) {

            /************************************
             *                                  *
             *               Init               *
             *                                  *
             ************************************/

            /**
             * Get original notes
             */

            $scope.notes = notes;

            /**
             * Init quick comment options
             */

            $scope.quickCommentOptions = ['文字字数不够', '文字不达标', '图片张数不够', '图片质量不达标', '刷评价'];

            /************************************
             *                                  *
             *              Events              *
             *                                  *
             ************************************/

            /**
             * Confirm
             */

            $scope.ok = function() {

                // Upload to the server
                $http.post('../api/web/?r=check/note',{
                        "commentid": commentid,
                        "executor": executor,
                        "notes": $scope.notes
                    }).
                    then(function successCallback(response) {

                        // Close the modal
                        $modalInstance.close($scope.status);
                    });
            };

            /**
             * Cancel
             */

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };

            /**
             * Add quick comment
             */

            $scope.addQuickComment = function(text) {
                $scope.notes = $scope.notes + text + '\n';
            };
        }
    ];

    return CheckModalController;
})
