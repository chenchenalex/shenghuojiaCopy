/**
 * CheckModel
 *
 * Used to provide data about Check.
 */

define([], function() {
    var CheckModel = ['$http', function($http) {

        /************************************
         *                                  *
         *               Get                *
         *                                  *
         ************************************/

        this.getList = function() {
            var data = $http.get('../api/web/?r=check/list').then(function success(response) {
                return checkListData = response.data;
            });
            return data;
        };

        this.getStatusTypes = function() {
            return statusTypes = [
                {label: '待审核', value: 0},
                {label: '已通过', value: 1},
                {label: '未通过', value: 2}
            ];
        };
    }];

    return CheckModel;
});