define([], function() {
    var AdminAvatorsModel = {
    	get: function() {
    		return [
    			{'id': 1, 'avatorSrc': 'img/avators/1.jpg'},
    			{'id': 2, 'avatorSrc': 'img/avators/2.jpg'},
    			{'id': 3, 'avatorSrc': 'img/avators/3.jpg'},
    			{'id': 4, 'avatorSrc': 'img/avators/4.jpg'},
    			{'id': 5, 'avatorSrc': 'img/avators/5.jpg'},
    			{'id': 6, 'avatorSrc': 'img/avators/6.jpg'},
    			{'id': 7, 'avatorSrc': 'img/avators/7.jpg'},
    		];
    	}
    };

    return AdminAvatorsModel;
});