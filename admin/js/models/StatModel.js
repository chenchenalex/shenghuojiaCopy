/**
 * StatModel
 *
 * Used to provide data about Check.
 */

define(['underscore'], function() {
    var StatModel = ['$http', function($http) {

        /************************************
         *                                  *
         *               Get                *
         *                                  *
         ************************************/

        this.getResult = function() {
            var data = $http.get('../api/web/?r=stat/index').then(function success(response) {
                var result = [];
                var typeList = _.keys(response.data);

                // 根据汇总类型，一项一项归并
                for (i in typeList) {
                    var type = typeList[i];
                    result[type] = [];

                    // 获取这项类型的数据（如daily的数据）
                    var dataForThisType = response.data[type];

                    // 获取所有字段，并按字段提取
                    var columns = _.keys(dataForThisType[0]);
                    for (j in columns) {
                        var column = columns[j];
                        result[type][column] = _.pluck(dataForThisType, column);
                    }
                }

                return result;
            });

            return data;
        };

        this.getFields = function() {
            return fields = [

                // UGC核心数据
                {
                    categoryName: 'UGC核心数据',
                    categoryItems: [{
                        'series': ['评价量'],
                        'dataLabels': ['countComment']
                    }, {
                        'series': ['认真评价量'],
                        'dataLabels': ['countCommentStarred']
                    }, {
                        'series': ['15字评价率'],
                        'dataLabels': ['ratioCommentLongerThan15']
                    },{
                        'series': ['晒图率'],
                        'dataLabels': ['ratioCommentHasPic']
                    }, {
                        'series': ['平均传图数'],
                        'dataLabels': ['avgCountCommentPic']
                    }]
                },

                // 生活家
                {
                    categoryName: '生活家',
                    categoryItems: [{
                        'series': ['总人数: 全国', '上海', '北京', '广州', '长沙'],
                        'dataLabels': [
                            'avgCountShjUserCumulative', 
                            'avgCountSHShjUserCumulative', 
                            'avgCountBJShjUserCumulative', 
                            'avgCountGZShjUserCumulative', 
                            'avgCountCSShjUserCumulative'
                        ]
                    },{
                        'series': ['产生入围人数: 全国', '上海', '北京', '广州', '长沙'],
                        'dataLabels': [
                            'avgCountShjUserCommented', 
                            'avgCountSHShjUserCommented', 
                            'avgCountBJShjUserCommented', 
                            'avgCountGZShjUserCommented', 
                            'avgCountCSShjUserCommented'
                        ]
                    },{
                        'series': ['产生精选人数: 全国', '上海', '北京', '广州', '长沙'],
                        'dataLabels': [
                            'avgCountShjUserCommentedStarred', 
                            'avgCountSHShjUserCommentedStarred', 
                            'avgCountBJShjUserCommentedStarred', 
                            'avgCountGZShjUserCommentedStarred', 
                            'avgCountCSShjUserCommentedStarred'
                        ]
                    },{
                        'series': ['产生POI人数: 全国', '上海', '北京', '广州', '长沙'],
                        'dataLabels': [
                            'avgCountShjUserCommentedPoi', 
                            'avgCountSHShjUserCommentedPoi', 
                            'avgCountBJShjUserCommentedPoi', 
                            'avgCountGZShjUserCommentedPoi', 
                            'avgCountCSShjUserCommentedPoi'
                        ]
                    },{
                        'series': ['入围评价量: 全国', '上海', '北京', '广州', '长沙'],
                        'dataLabels': [
                            'avgCountShjComment', 
                            'avgCountSHShjComment', 
                            'avgCountBJShjComment', 
                            'avgCountGZShjComment', 
                            'avgCountCSShjComment'
                        ]
                    },{
                        'series': ['精选评价量: 全国', '上海', '北京', '广州', '长沙'],
                        'dataLabels': [
                            'avgCountShjCommentStarred', 
                            'avgCountSHShjCommentStarred', 
                            'avgCountBJShjCommentStarred', 
                            'avgCountGZShjCommentStarred', 
                            'avgCountCSShjCommentStarred'
                        ]
                    },{
                        'series': ['POI入围量: 全国', '上海', '北京', '广州', '长沙'],
                        'dataLabels': [
                            'avgCountShjCommentPoi', 
                            'avgCountSHShjCommentPoi', 
                            'avgCountBJShjCommentPoi', 
                            'avgCountGZShjCommentPoi', 
                            'avgCountCSShjCommentPoi'
                        ]
                    },{
                        'series': ['POI精选量: 全国', '上海', '北京', '广州', '长沙'],
                        'dataLabels': [
                            'avgCountShjCommentPoiStarred', 
                            'avgCountSHShjCommentPoiStarred', 
                            'avgCountBJShjCommentPoiStarred', 
                            'avgCountGZShjCommentPoiStarred', 
                            'avgCountCSShjCommentPoiStarred'
                        ]
                    },]
                },
                
                // 认真评价
                {
                    categoryName: '认真评价',
                    categoryItems: [{
                        'series': ['认真评价量'],
                        'dataLabels': ['countCommentStarred']
                    }, {
                        'series': ['总体认真评价率'],
                        'dataLabels': ['ratioCommentStarred']
                    }, {
                        'series': ['认真评价审核通过率'],
                        'dataLabels': ['ratioCommentPassStarCheck']
                    }, {
                        'series': ['100字4图/200字评价率'],
                        'dataLabels': ['ratioCommentEnterStarCheck']
                    }, {
                        'series': ['100字评价率'],
                        'dataLabels': ['ratioCommentLongerThan100']
                    }, {
                        'series': ['200字评价率'],
                        'dataLabels': ['ratioCommentLongerThan200']
                    }, {
                        'series': ['4图评价率'],
                        'dataLabels': ['ratioCommentCountPicMoreThan4']
                    }]
                },

                //评价转化
                {
                    categoryName: '评价转化',
                    categoryItems: [{
                        'series': ['评价转化率 (当天)'],
                        'dataLabels': ['ratioCommentedIn1day']
                    },{
                        'series': ['评价转化率 (第13天)'],
                        'dataLabels': ['ratioCommentedAt13day']
                    },{
                        'series': ['评价转化率 (第28天)'],
                        'dataLabels': ['ratioCommentedAt28day']
                    },{
                        'series': ['评价转化率: 整体','当天','第1~3天','第3~7天','第7~30天','第30天之后'],
                        'dataLabels': ['ratioCommented','ratioCommentedIn1day','ratioCommentedIn3days','ratioCommentedIn7days','ratioCommentedIn30days','ratioCommentedAfter30days']
                    }]
                },

                // 平台业务
                {
                    categoryName: '平台业务',
                    categoryItems: [{
                        'series': ['总消费量（验券+买单）', '验券量', '买单量'],
                        'dataLabels': ['countAllConsumption', 'countCoupon', 'countPay']
                    }]
                }
            ];
        };
    }];

    return StatModel;
});
