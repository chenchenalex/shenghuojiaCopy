/**
 * LinkModel
 *
 * Used to provide data about links.
 */

define([], function() {
    var LinkModel = ['$http', function($http) {

        /************************************
         *                                  *
         *               Get                *
         *                                  *
         ************************************/

        this.getLinkList = function() {
            return [
                {'name': 'MIS后台', 'href': 'http://mis.sankuai.com/mis/'},
                {'name': '生活家信息导入（石墨）', 'href': 'https://shimo.im/doc/vjo1JIMGk8JlS1ra'},
                {'name': '用户反馈（金数据）', 'href': 'https://jinshuju.net/forms/oCBlCz'}
            ];
        };
    }];

    return LinkModel;
});