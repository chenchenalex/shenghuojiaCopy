/**
 * PointModel
 *
 * Used to connect with backend API of point.
 */

define([], function() {
    var PointModel = ['$http', function($http) {

        /************************************
         *                                  *
         *               Get                *
         *                                  *
         ************************************/

        this.getList = function() {
            var data = $http.get('../api/web/?r=point/list').then(function success(response) {
                return pointListData = response.data;
            });
            return data;
        };
    }];

    return PointModel;
});