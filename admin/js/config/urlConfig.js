define([],function(){
	var UrlConfig = ['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/login');
        
        $stateProvider
            .state('check', {
                url: '/check',
                templateUrl: 'html/check.html',
                controller: 'CheckController'
            })
            .state('point', {
                url: '/point',
                templateUrl: 'html/point.html',
                controller: 'PointController'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'html/login.html',
                controller: 'LoginController'
            })
            .state('userList', {
                url: '/user',
                templateUrl: 'html/userList.html',
                controller: 'UserListController'
            })
            .state('userDetail', {
                url: '/user/:id',
                templateUrl: 'html/userDetail.html',
                controller: 'UserDetailController'
            })
            .state('import', {
                url: '/import',
                templateUrl: 'html/import.html',
                controller: 'ImportController'
            })
            .state('settings', {
                url: '/settings',
                templateUrl: 'html/settings.html',
                controller: 'SettingsController'
            })
            .state('link', {
                url: '/link',
                templateUrl: 'html/link.html',
                controller: 'LinkController'
            })
            .state('stat', {
                url: '/stat',
                templateUrl: 'html/stat.html',
                controller: 'StatController'
            })
    }]; 
		
    return UrlConfig;
})