<?php

namespace app\functions;

class StringFunctions
{
    public static function formatMonth($originalMonth, $format) 
    {
    	switch ($format)
    	{
    		case 'm':
    			if ($originalMonth < 10) {
	    			$formatedMonth = '0'.$originalMonth; 
	    		} else {
	    			$formatedMonth = $originalMonth;
	    		}
    			break;
    		
    		default:
    			$formatedMonth = $originalMonth;
    			break;
    	}

    	return $formatedMonth;
    }
}