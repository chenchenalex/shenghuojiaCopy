<?php

namespace app\functions;

class CookieFunctions
{
	/* 设置Cookies */
	public function setCookie($key, $value) {
	    $cookies = \Yii::$app->response->cookies;

        $cookies->add(new \yii\web\Cookie([
            'name' => $key,
            'value' => $value,
        ]));
	}
}