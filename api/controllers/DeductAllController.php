<?php

namespace app\controllers;

use app\models\Point;
use app\controllers\PointController;

\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

class DeductAllController extends \yii\web\Controller
{
    public function actionIndex(
    	$fine = -1000, 
    	$notes = '扣除基础奖学金',
    	$executor = 'system'
    ){
    	foreach (Point::find()->all() as $user) {
    	    PointController::update($user->id, $fine, $notes, $executor);
    	}

    	echo 'Done!';
    }
}
