<?php

namespace app\controllers;

use app\models\CheckLog;
use app\models\PointLog;
use app\models\TaskOneLog;

\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

class LogController extends \yii\web\Controller
{
    public function actionUser()
    {
        $id = \Yii::$app->request->post('id');

        return [
        	'checkLog' => self::getCheckLogByUser($id),
        	'pointLog' => self::getPointLogByUser($id),
        	'taskOneLog' => self::getTaskOneLogByUser($id)
        ];
    }

    public static function getCheckLogByUser($id)
    {
    	$id = \Yii::$app->request->post('id');

    	$sql = 'select shj_checkLog.logid, shj_checkStatus.id, shj_checkLog.commentid, shj_checkLog.checkStatus, shj_checkLog.notes, shj_checkLog.executor, shj_checkLog.createTime
    	        from shj_checkLog, shj_checkStatus
    	        where shj_checkStatus.id = '.$id.'
    	        and shj_checkLog.commentid = shj_checkStatus.commentid';
    	$connection = \Yii::$app->db;
    	$results = $connection->createCommand($sql)->queryAll();

    	return $results;
    }

    public static function getPointLogByUser($id)
    {
    	return $table = PointLog::findAll(['id' => $id]);
    }

    public static function getTaskOneLogByUser($id)
    {
    	return $table = TaskOneLog::findAll(['id' => $id]);
    }
}
