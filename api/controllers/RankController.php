<?php

namespace app\controllers;

use app\models\User;
use app\functions\StringFunctions;

\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

class RankController extends \yii\web\Controller
{
	public function actionTest() 
	{
		return self::getRankByUser(4, 10);
	}

    public function actionCity()
    {
        $post = \Yii::$app->request->post();
        	$city = $post['city'];
        	$month = $post['month'];

        return $rankList = self::getRankListByCity($city, $month);
    }

    public static function getRankListByCity($city, $month) 
    {
    	$formatedMonth = StringFunctions::formatMonth($month, 'm');

    	$sql = 'select  shj_checkStatus.id, name, city, count(commentid) as count
    	        from    shj_checkStatus, shj_user  
    	        where   shj_checkStatus.id = shj_user.id
    	        '.self::getSQLCityPart($city).'
    	        and     substr(commentDate, 5, 2) = '.$formatedMonth.'
    	        and     checkStatus = 1
                and     city != "芜湖"
    	        group by id
    	        order by count desc, sum(commentDate)';
    	$connection = \Yii::$app->db;
    	$rankList = $connection->createCommand($sql)->queryAll();

    	return $rankList;
    }

    public static function getRankByUser($id, $month) 
    {
    	$city = User::findOne($id)->city;

    	$rankListByCity = self::getRankListByCity($city, $month);

    	$rank = 0;
    	foreach ($rankListByCity as $key => $value) {
    	    if ($value['id'] == $id) {
    	        $rank = $key + 1;
    	        break;
    	    }
    	}
    	
    	return $rank;
    }

    public static function getSQLCityPart($city) {
        if ($city == '全国') {
            $str = '';
        }
        else {
            $str = 'and city = "'.$city.'"';
        }

        return $str;
    }
}
