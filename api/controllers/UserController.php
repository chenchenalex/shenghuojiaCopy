<?php

namespace app\controllers;

use app\models\User;
use app\models\CheckStatus;
use app\models\Point;
use app\models\TaskOne;
use app\controllers\RankController;
use app\functions\StringFunctions;
use app\functions\CookieFunctions;

\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

class UserController extends \yii\web\Controller
{
    public function actionOverview() {
        $post = \Yii::$app->request->post();
            $id = $post['id'];
            $month = $post['month'];

        return self::getOverview($id, $month);
    }

    public function actionList() {
        $sql = 'select id, vipid, name, city, tel
                from shj_user
                order by id';
        $userList = User::findBySql($sql)->all();

        return $userList;
    }

    public static function actionInfo() {
        $id = \Yii::$app->request->post('id');
        $info = self::getInfo($id);

        return [
            'id' => $info->id,
            'vipid' => $info->vipid,
            'name' => $info->name,
            'city' => $info->city,
            'tel' => $info->tel,
            'joinDate' => $info->joinDate
        ];
    }

    public function actionLogin() {
        $request = \Yii::$app->request;
        
        if ($request->isGet) {
            $tel = self::getLoginCookies();

            if ($tel) {
                return self::returnUserInfo($tel);
            } 
            else {
                return self::returnError('无法自动登录');
            }
        }

        if ($request->isPost) {
            $tel = $request->post('tel');

            if (self::findTel($tel)) {
                self::saveLoginCookies($tel);
                return self::returnUserInfo($tel);
            } 
            else {
                self::clearLoginCookies();
                return self::returnError('没有找到你的手机号哦');
            }
        }
    }

    public function actionLogout() {
        self::clearLoginCookies();
    }



    /* ----------------------------- */



    public static function getOverview($id, $month) {
        $info = self::getInfo($id);
        $check = self::getCheck($id, $month);
        $point = self::getPoint($id);
        $rank = self::getRank($id, $month);
        $stage = self::getStage($id, $month);
        $lastUpdateDate = self::getLastUpdateDate();

        return [
            'info' => ['name' => $info->name],
            'check' => $check,
            'point' => $point,
            'rank' => $rank,
            'stage' => $stage,
            'lastUpdateDate' => $lastUpdateDate
        ];
    }

    public static function getInfo($id) {
        return $info = User::findOne($id);
    }

    public static function getCheck($id, $month) {
        $checkUnchecked = self::getCheckByStatus($id, $month, 0);
        $checkApproved = self::getCheckByStatus($id, $month, 1);
        $checkDisapproved = self::getCheckByStatus($id, $month, 2);

        return $check = [
            'approved' => $checkApproved,
            'disapproved' => $checkDisapproved,
            'unchecked' => $checkUnchecked
        ];
    }

    public static function getCheckByStatus($id, $month, $checkStatus) {
        $formatedMonth = StringFunctions::formatMonth($month, 'm');

        $sql = 'select shopName, checkDate
                from shj_checkStatus
                where id = '.$id.'
                and substr(commentDate, 5, 2) = '.$formatedMonth.'
                and checkStatus = '.$checkStatus.'
                order by commentDate';
        $connection = \Yii::$app->db;
        $results = $connection->createCommand($sql)->queryAll();

        return $results;
    }

    public static function getPoint($id) {
        return $point = Point::findOne($id)->point;
    }

    public static function getRank($id, $month) {
        return RankController::getRankByUser($id, $month);
    }

    public static function getStage($id, $month) {
        $row = TaskOne::findOne(['id'=>$id, 'month'=>$month]);

        if ($row) {
            $stage = $row->stage;
        }
        else {
            $stage = 0;
        }

        return $stage;
    }

    public static function getLastUpdateDate() {
        $time = time() - 1 * 24 * 60 * 60;
        $lastUpdateDate = date('Ymd', $time);

        return $lastUpdateDate;
    }



    /* ----------------------------- */
    


    public static function returnUserInfo($tel) {
        $userInfoBasic = self::getUserInfoBasic($tel);

        return [
            'status' => [
                'code' => 1
            ],
            'user' => $userInfoBasic
        ];
    }

    public static function returnError($errorMsg = '') {
        return [
            'status' => [
                'code' => 0,
                'text' => $errorMsg
            ],
            'user' => []
        ];
    }

    public static function getUserInfoBasic($tel) {
        $userInfoFull = User::findOne(['tel' => $tel]);

        return $userInfoBasic = [
            'id' => $userInfoFull->id,
            'name' => $userInfoFull->name,
            'city' => $userInfoFull->city
        ];
    } 

    public static function findTel($tel) {
        return User::findOne(['tel' => $tel]);
    }

    public static function getLoginCookies() {
        $cookies = \Yii::$app->request->cookies;

        if ($cookies->has('userTel')){
            return $tel = $cookies['userTel']->value;
        }
        else {
            return false;
        }
    }

    public static function saveLoginCookies($tel) {
        CookieFunctions::setCookie('userTel', $tel);
    }

    public static function clearLoginCookies() {
        $cookies = \Yii::$app->response->cookies;
        $cookies->remove('userTel');
    }
}
