<?php

namespace app\controllers;

use app\models\User;

\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

class LoginController extends \yii\web\Controller
{
    /* 
        Actions 
    */

    /* 登录 */
    public function actionLogin()
    {
        $request = \Yii::$app->request;
        
        if ($request->isGet) 
        {
            if ($this->hasCookies()) 
            {
                $cookies = \Yii::$app->request->cookies;
                return $this->verifyPass($cookies['id'], $cookies['pass']);

            } else {

                return $this->denyLogin();
            }
        }

        if ($request->isPost) 
        {
            $post = $request->post();
            return $this->verifyPass($post['id'], $post['pass']);
        }
    }

    /* 登出 */
    public function actionLogout()
    {
        $cookies = \Yii::$app->response->cookies;
        $cookies->remove('id');
        $cookies->remove('pass');
    }



    /* 
        Functions 
    */

    /* 验证密码 */
    protected function verifyPass($id, $pass) 
    {
        if ($this->passIsCorrect($id, $pass)) 
        {
            if (!$this->hasCookies()) {
                $this->saveLoginStatus($id, $pass);
            } 
            
            return $this->acceptLogin($id);

        } else {

            return $this->denyLogin();
        }
    }

    /* 登录成功 */
    protected function acceptLogin($id) 
    {
        $user = $this->getUserInfo($id);

        return [
            'status' => 1,
            'admin' => [
                'id' => $user->id,
                'name' => $user->name,
                'city' => $user->city,
            ]
        ];
    }

    /* 登录失败 */
    protected function denyLogin() 
    {
        return [
            'status' => 0
        ];
    }

    /* 检查是否已有cookies */
    protected function hasCookies() 
    {
        $cookies = \Yii::$app->request->cookies;
        return $cookies->has('id') && $cookies->has('pass');
    }

    /* 获取指定用户全部信息 */
    protected function getUserInfo($id) {
        return User::findOne($id);
    }

    /* 保存cookies */
    protected function saveLoginStatus($id, $pass) 
    {
        $this->setCookie('id', $id);
        $this->setCookie('pass', $pass);
    }

    /* 验证密码 */
    protected function passIsCorrect($id, $pass) 
    {
        $passInput = hash('md5', $pass);
        $passCorrect = $this->getUserInfo($id)->pass;

        return $passInput == $passCorrect;
    }

    /* 设置Cookies */
    protected function setCookie($key, $value) 
    {
        $cookies = \Yii::$app->response->cookies;

        $cookies->add(new \yii\web\Cookie([
            'name' => $key,
            'value' => $value,
        ]));
    }
}
