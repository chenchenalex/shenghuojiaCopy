<?php

namespace app\controllers;

use app\models\CheckStatus;
use app\models\PointLog;
use app\controllers\PointController;

\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

class TaskThreeController extends \yii\web\Controller
{
    public function actionPunish($month, $minCountComments = 3, $fine = -600)
    {
        // 把不满足要求（该月不满3条入围评价）的人拿出来，排好队
    	$users = self::getUsersFailTaskThree($month, $minCountComments);

        // 一个一个扣600奖学金
    	foreach ($users as $key => $userid) {

    		// 检查之前是否扣过
    		$pointLog = PointLog::findOne([
				'id' => $userid, 
				'notes' => $month.'月未满3条入围评价'
			]);

    		// 如果没扣过，就扣
    		if (!isset($pointLog)) {
    			PointController::update($userid, $fine, $month.'月未满3条入围评价', 'system');
    		}
    	}

    	// 返回状态
    	echo 'Done!';
    }

    public function getUsersFailTaskThree($month, $minCountComments) 
    {
    	// 把不符合要求的用户跑出来
    	$sql = 'select 	user.userid as userid,
    					countComments

    			from (
    				select 	id as userid
					from 	shj_user 
    			)
				as user
				
				left join (
					select 	id as userid,
							count(commentid) as countComments
					from 	shj_checkStatus
					where 	substr(commentDate, 1, 6) = "'.$month.'"
					group by id
				)
				as checkStatus
				on user.userid = checkStatus.userid
    			
    			where	countComments is null
    					or countComments < '.$minCountComments;

    	$connection = \Yii::$app->db;
    	$results = $connection->createCommand($sql)->queryAll();

    	// 把结果转成userid列表
    	$users = array_column($results, 'userid');

    	// 返回
    	return $users;
    }
}
