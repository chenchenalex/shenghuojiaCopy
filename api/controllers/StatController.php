<?php

namespace app\controllers;

use app\models\UgcDaily;

\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

class StatController extends \yii\web\Controller
{
    public function actionIndex() {

        $modeList = ['daily', 'weekly', 'monthly', 'yearly'];

        foreach ($modeList as $key => $mode) {
            $results[$mode] = self::getResultsBy($mode);
        }

        return $results;
    }

    public function getResultsBy($mode) {

        // 跑SQL，拿结果
        $sql = self::getFullSQL($mode);
        $connection = \Yii::$app->db;
        $results = $connection->createCommand($sql)->queryAll();

        // 对结果进一步处理
        switch ($mode) {
            
            // 如果是weekly，则需要对period重新排序
            case 'weekly':
                // 改变week格式：YYYY-W -> YYYY-WW
                foreach ($results as $key => $value) {
                    $year = substr($value['period'], 0, 4);
                    $week = substr($value['period'], 5);
                    if ($week <= 9) {
                        $results[$key]['period'] = $year.'-0'.$week;
                    }
                }
                // 按新week格式排序
                usort($results, array($this, 'compareWeek'));
                break;
        }

        return $results;
    }

    public static function getSQLDatePart($mode) {
        switch ($mode) {
            case 'daily':
                $dateStr = 'dateList.date';
                break;

            case 'weekly':
                $dateStr = "concat(
                                substr(dateList.date, 1, 4), '-',
                                weekofyear(
                                    concat(
                                        substr(dateList.date, 1, 4), '-', 
                                        substr(dateList.date, 5, 2), '-', 
                                        substr(dateList.date, 7, 2)
                                    )
                                )
                            )";
                break;

            case 'monthly':
                $dateStr = "substr(dateList.date, 1, 6)";
                break;

            case 'yearly':
                $dateStr = "substr(dateList.date, 1, 4)";
                break;
        }

        return $dateStr;
    }

    public function compareWeek($a, $b) {
        if ($a['period'] == $b['period']) {
            return 0;
        }
        return ($a['period'] < $b['period']) ? -1 : 1;
    }

    public static function getFullSQL($mode) {
        $SQLDatePart = self::getSQLDatePart($mode);

        return $sql = " select ".$SQLDatePart." as period,
                        round(avg(countAllConsumption),4) as countAllConsumption,
                        round(avg(countCoupon),4) as countCoupon,
                        round(avg(countPay),4) as countPay,
                        round(avg(countComment),4) as countComment,
                        round(avg(countCommentStarred),4) as countCommentStarred,
                        round(avg(ratioCommented),4) as ratioCommented,
                        round(avg(ratioCommentedIn1day),4) as ratioCommentedIn1day,
                        round(avg(ratioCommentedIn3days),4) as ratioCommentedIn3days,
                        round(avg(ratioCommentedIn7days),4) as ratioCommentedIn7days,
                        round(avg(ratioCommentedIn30days),4) as ratioCommentedIn30days,
                        round(avg(ratioCommentedAfter30days),4) as ratioCommentedAfter30days,
                        round(avg(ratioCommentedAt13day),4) as ratioCommentedAt13day,
                        round(avg(ratioCommentedAt28day),4) as ratioCommentedAt28day,
                        round(avg(ratioCommentLongerThan15),4) as ratioCommentLongerThan15,
                        round(avg(ratioCommentLongerThan100),4) as ratioCommentLongerThan100,
                        round(avg(ratioCommentLongerThan200),4) as ratioCommentLongerThan200,
                        round(avg(ratioCommentHasPic),4) as ratioCommentHasPic,
                        round(avg(ratioCommentCountPicMoreThan4),4) as ratioCommentCountPicMoreThan4,
                        round(avg(avgCountCommentPic),4) as avgCountCommentPic,
                        round(avg(ratioCommentEnterStarCheck),4) as ratioCommentEnterStarCheck,
                        round(avg(ratioCommentPassStarCheck),4) as ratioCommentPassStarCheck,
                        round(avg(ratioCommentStarred),4) as ratioCommentStarred,

                        round(avg(countShjUserCumulative),2) as avgCountShjUserCumulative,
                        round(avg(countShjUserCommented),2) as avgCountShjUserCommented,
                        round(avg(countShjUserCommentedStarred),2) as avgCountShjUserCommentedStarred,
                        round(avg(countShjUserCommentedPoi),2) as avgCountShjUserCommentedPoi,
                        round(avg(countShjComment),2) as avgCountShjComment,
                        round(avg(countShjCommentStarred),2) as avgCountShjCommentStarred,
                        round(avg(countShjCommentPoi),2) as avgCountShjCommentPoi,
                        round(avg(countShjCommentPoiStarred),2) as avgCountShjCommentPoiStarred,

                        round(avg(countSHShjUserCumulative),2) as avgCountSHShjUserCumulative,
                        round(avg(countSHShjUserCommented),2) as avgCountSHShjUserCommented,
                        round(avg(countSHShjUserCommentedStarred),2) as avgCountSHShjUserCommentedStarred,
                        round(avg(countSHShjUserCommentedPoi),2) as avgCountSHShjUserCommentedPoi,
                        round(avg(countSHShjComment),2) as avgCountSHShjComment,
                        round(avg(countSHShjCommentStarred),2) as avgCountSHShjCommentStarred,
                        round(avg(countSHShjCommentPoi),2) as avgCountSHShjCommentPoi,
                        round(avg(countSHShjCommentPoiStarred),2) as avgCountSHShjCommentPoiStarred,

                        round(avg(countBJShjUserCumulative),2) as avgCountBJShjUserCumulative,
                        round(avg(countBJShjUserCommented),2) as avgCountBJShjUserCommented,
                        round(avg(countBJShjUserCommentedStarred),2) as avgCountBJShjUserCommentedStarred,
                        round(avg(countBJShjUserCommentedPoi),2) as avgCountBJShjUserCommentedPoi,
                        round(avg(countBJShjComment),2) as avgCountBJShjComment,
                        round(avg(countBJShjCommentStarred),2) as avgCountBJShjCommentStarred,
                        round(avg(countBJShjCommentPoi),2) as avgCountBJShjCommentPoi,
                        round(avg(countBJShjCommentPoiStarred),2) as avgCountBJShjCommentPoiStarred,

                        round(avg(countGZShjUserCumulative),2) as avgCountGZShjUserCumulative,
                        round(avg(countGZShjUserCommented),2) as avgCountGZShjUserCommented,
                        round(avg(countGZShjUserCommentedStarred),2) as avgCountGZShjUserCommentedStarred,
                        round(avg(countGZShjUserCommentedPoi),2) as avgCountGZShjUserCommentedPoi,
                        round(avg(countGZShjComment),2) as avgCountGZShjComment,
                        round(avg(countGZShjCommentStarred),2) as avgCountGZShjCommentStarred,
                        round(avg(countGZShjCommentPoi),2) as avgCountGZShjCommentPoi,
                        round(avg(countGZShjCommentPoiStarred),2) as avgCountGZShjCommentPoiStarred,

                        round(avg(countCSShjUserCumulative),2) as avgCountCSShjUserCumulative,
                        round(avg(countCSShjUserCommented),2) as avgCountCSShjUserCommented,
                        round(avg(countCSShjUserCommentedStarred),2) as avgCountCSShjUserCommentedStarred,
                        round(avg(countCSShjUserCommentedPoi),2) as avgCountCSShjUserCommentedPoi,
                        round(avg(countCSShjComment),2) as avgCountCSShjComment,
                        round(avg(countCSShjCommentStarred),2) as avgCountCSShjCommentStarred,
                        round(avg(countCSShjCommentPoi),2) as avgCountCSShjCommentPoi,
                        round(avg(countCSShjCommentPoiStarred),2) as avgCountCSShjCommentPoiStarred


                from (
                    select date from ugc_daily
                    union
                    select distinct commentDate as date from shj_checkStatus
                )
                as dateList


                left join ugc_daily 
                as  ugcTable 
                on  dateList.date = ugcTable.date


                left join (

                    select  commentDate 
                                as date,

                            count(distinct shj_checkStatus.id) 
                                as countShjUserCommented,
                            count(distinct (case when (checkStatus = 1) then shj_checkStatus.id else null end)) 
                                as countShjUserCommentedStarred,
                            count(distinct (case when (source = 'POI') then shj_checkStatus.id else null end)) 
                                as countShjUserCommentedPoi,
                            count(commentid) 
                                as countShjComment,
                            count(case when (checkStatus = 1) then 1 else null end) 
                                as countShjCommentStarred,
                            count(case when (source = 'POI') then 1 else null end) 
                                as countShjCommentPoi,
                            count(case when (source = 'POI' and checkStatus = 1) then 1 else null end) 
                                as countShjCommentPoiStarred,

                            count(distinct (case when (city = '上海') then shj_checkStatus.id else null end)) 
                                as countSHShjUserCommented,
                            count(distinct (case when (city = '上海' and checkStatus = 1) then shj_checkStatus.id else null end)) 
                                as countSHShjUserCommentedStarred,
                            count(distinct (case when (city = '上海' and source = 'POI') then shj_checkStatus.id else null end)) 
                                as countSHShjUserCommentedPoi,
                            count(case when (city = '上海') then commentid else null end) 
                                as countSHShjComment,
                            count(case when (city = '上海' and checkStatus = 1) then 1 else null end) 
                                as countSHShjCommentStarred,
                            count(case when (city = '上海' and source = 'POI') then 1 else null end) 
                                as countSHShjCommentPoi,
                            count(case when (city = '上海' and source = 'POI' and checkStatus = 1) then 1 else null end) 
                                as countSHShjCommentPoiStarred,

                            count(distinct (case when (city = '北京') then shj_checkStatus.id else null end)) 
                                as countBJShjUserCommented,
                            count(distinct (case when (city = '北京' and checkStatus = 1) then shj_checkStatus.id else null end)) 
                                as countBJShjUserCommentedStarred,
                            count(distinct (case when (city = '北京' and source = 'POI') then shj_checkStatus.id else null end)) 
                                as countBJShjUserCommentedPoi,
                            count(case when (city = '北京') then commentid else null end) 
                                as countBJShjComment,
                            count(case when (city = '北京' and checkStatus = 1) then 1 else null end) 
                                as countBJShjCommentStarred,
                            count(case when (city = '北京' and source = 'POI') then 1 else null end) 
                                as countBJShjCommentPoi,
                            count(case when (city = '北京' and source = 'POI' and checkStatus = 1) then 1 else null end) 
                                as countBJShjCommentPoiStarred,

                            count(distinct (case when (city = '广州') then shj_checkStatus.id else null end)) 
                                as countGZShjUserCommented,
                            count(distinct (case when (city = '广州' and checkStatus = 1) then shj_checkStatus.id else null end)) 
                                as countGZShjUserCommentedStarred,
                            count(distinct (case when (city = '广州' and source = 'POI') then shj_checkStatus.id else null end)) 
                                as countGZShjUserCommentedPoi,
                            count(case when (city = '广州') then commentid else null end) 
                                as countGZShjComment,
                            count(case when (city = '广州' and checkStatus = 1) then 1 else null end) 
                                as countGZShjCommentStarred,
                            count(case when (city = '广州' and source = 'POI') then 1 else null end) 
                                as countGZShjCommentPoi,
                            count(case when (city = '广州' and source = 'POI' and checkStatus = 1) then 1 else null end) 
                                as countGZShjCommentPoiStarred,

                            count(distinct (case when (city = '长沙') then shj_checkStatus.id else null end)) 
                                as countCSShjUserCommented,
                            count(distinct (case when (city = '长沙' and checkStatus = 1) then shj_checkStatus.id else null end)) 
                                as countCSShjUserCommentedStarred,
                            count(distinct (case when (city = '长沙' and source = 'POI') then shj_checkStatus.id else null end)) 
                                as countCSShjUserCommentedPoi,
                            count(case when (city = '长沙') then commentid else null end) 
                                as countCSShjComment,
                            count(case when (city = '长沙' and checkStatus = 1) then 1 else null end) 
                                as countCSShjCommentStarred,
                            count(case when (city = '长沙' and source = 'POI') then 1 else null end) 
                                as countCSShjCommentPoi,
                            count(case when (city = '长沙' and source = 'POI' and checkStatus = 1) then 1 else null end) 
                                as countCSShjCommentPoiStarred

                    from    shj_checkStatus

                    join    shj_user
                    on      shj_checkStatus.id = shj_user.id

                    group by date
                ) 
                as shjCheckTable
                on dateList.date = shjCheckTable.date


                left join (
                    select  dateList.date as date,
                            sum(countShjUserNew) as countShjUserCumulative,
                            sum(countSHShjUserNew) as countSHShjUserCumulative,
                            sum(countBJShjUserNew) as countBJShjUserCumulative,
                            sum(countGZShjUserNew) as countGZShjUserCumulative,
                            sum(countCSShjUserNew) as countCSShjUserCumulative

                    from (
                        select date from ugc_daily
                        union
                        select distinct commentDate as date from shj_checkStatus
                    )
                    as dateList

                    left join (
                        select  joinDate as date,
                                count(id) as countShjUserNew,
                                count(case when city = '上海' then id else null end) as countSHShjUserNew,
                                count(case when city = '北京' then id else null end) as countBJShjUserNew,
                                count(case when city = '广州' then id else null end) as countGZShjUserNew,
                                count(case when city = '长沙' then id else null end) as countCSShjUserNew

                        from    shj_user
                        group by date
                    )
                    as joinTable
                    on dateList.date >= joinTable.date

                    group by date
                )
                as shjUserTable
                on dateList.date = shjUserTable.date


                group by period";
    }
}
