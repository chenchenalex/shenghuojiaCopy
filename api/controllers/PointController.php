<?php

namespace app\controllers;

use app\models\Point;
use app\models\PointLog;

\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

class PointController extends \yii\web\Controller
{
    public function actionList()
    {
        $sql = 'select shj_point.id, name, city, point
                from shj_point, shj_user 
                where shj_point.id = shj_user.id
                order by shj_point.id';
        $connection = \Yii::$app->db;
        $results = $connection->createCommand($sql)->queryAll();
        
        return $results;
    }

    public function actionUpdate()
    {
    	$post = \Yii::$app->request->post();
    	   $id = $post['id']; 
           $delta = $post['delta']; 
           $notes = $post['notes']; 
           $executor = $post['executor'];

        self::update($id, $delta, $notes, $executor);
    }

    public static function update($id, $delta, $notes, $executor) 
    {
        // 计算要更新成多少积分
        $table = Point::findOne($id);
            $pointNow = $table->point;
            $pointUpdated = $pointNow + $delta;

        // 最多积分扣成0
        if ($pointUpdated < 0) {
            $pointUpdated = 0;
            $delta = - $pointNow;
        }

        // 写入积分表
        $table->point = $pointUpdated;
        $table->save();

        // 写入积分明细表
        $record = new PointLog();
            $record->id = $id;
            $record->delta = $delta;
            $record->notes = $notes;
            $record->executor = $executor;
        $record->save();
    }
}
