<?php

namespace app\controllers;

use app\models\TaskTwo;
use app\models\TaskTwoLog;

class TaskTwoController extends \yii\web\Controller
{
    public static function happen($id, $month)
    {
        if (!self::hasStarredCommentAlready($id, $month)) 
        {
            // 记录在TaskTwo中
            self::recordInTaskTwoDatabase($id, $month);

            // 发放积分
            self::givePoint($id, $month);
        }
    }

    public static function hasStarredCommentAlready($id, $month) 
    {
    	$result = TaskTwo::findOne([
    		'id' => $id,
    		'month' => $month
    	]);

    	if ($result) {
    	    return true;
    	} 
    	else {
    	    return false;
    	}
    }

    public static function recordInTaskTwoDatabase($id, $month) 
    {
    	// 记录在TaskTwo中
    	$record = new TaskTwo();
    	    $record->id = $id;
    	    $record->month = $month;
    	    $record->hasStarredComment = 1;
    	$record->save();

    	// 记录在TaskTwoLog中
		$record = new TaskTwoLog();
	        $record->id = $id;
	        $record->month = $month;
	        $record->hasStarredComment = 1;
	        $record->executor = 'System';
	    $record->save();
    }

    public static function givePoint($id, $month) 
    {
    	$pointDelta = 100;
    	$notes = $month.'月第1次被评为精选评价';
    	$executor = 'System';
    	PointController::update($id, $pointDelta, $notes, $executor);
    }
}
