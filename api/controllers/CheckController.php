<?php

namespace app\controllers;

use app\models\CheckStatus;
use app\models\CheckLog;
use app\controllers\TaskOneController;
use app\controllers\TaskTwoController;
use app\functions\StringFunctions;

\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

class CheckController extends \yii\web\Controller
{
    public function actionList()
    {
        $sql = 'select      shj_user.id, 
                            vipid, 
                            name, 
                            tel, 
                            city, 
                            commentid, 
                            orderid, 
                            commentDate, 
                            shopName, 
                            source, 
                            commentContent, 
                            wordCount, 
                            imageCount, 
                            checkStatus, 
                            score, 
                            notes, 
                            executor, 
                            checkDate, 
                            importDate

                from        shj_user, 
                            shj_checkStatus

                where       shj_checkStatus.id = shj_user.id

                order by    commentDate desc, 
                            commentid desc';
                            
        $connection = \Yii::$app->db;
        $results = $connection->createCommand($sql)->queryAll();

        // Convert string to integer
        foreach ($results as $key => $value) {
            $results[$key]['score'] = (int) $value['score'];
            $results[$key]['wordCount'] = (int) $value['wordCount'];
            $results[$key]['imageCount'] = (int) $value['imageCount'];
            $results[$key]['checkStatus'] = (int) $value['checkStatus'];
        }

        return $results;
    }

    public function actionUpdate()
    {
        // 获取post
        $post = \Yii::$app->request->post();
            $commentid = $post['commentid'];
            $checkStatus = $post['checkStatus'];
            $executor = $post['executor'];

        // 读取审核表
        $table = CheckStatus::findOne(['commentid' => $commentid]);

            // 更新审核结果
            $table->executor = $executor;
            $table->checkStatus = $checkStatus;
            $table->checkDate = date('Ymd');
            $table->save();

            // 提取其他字段，供后面使用
            $id = $table->id;
            $commentDate = $table->commentDate;
            $month = substr($commentDate, 4, 2);
            $month = StringFunctions::formatMonth($month, 'm');

        // 写入审核明细
        $record = new CheckLog();
            $record->commentid = $commentid;
            $record->checkStatus = $checkStatus;
            $record->executor = $executor;
        $record->save();

        // 如果审核通过，则触发任务1（每月评价数量阶段）
        if ($checkStatus == 1) {
            TaskOneController::happen($id, $month);
            TaskTwoController::happen($id, $month);
        }
    }

    public function actionScore()
    {
        // 获取post
        $post = \Yii::$app->request->post();
            $commentid = $post['commentid'];
            $executor = $post['executor'];
            $score = $post['score'];

        // 更新审核结果
        $table = CheckStatus::findOne(['commentid' => $commentid]);
            $table->score = $score;
            $table->executor = $executor;
            $table->checkDate = date('Ymd');
        $table->save();

        // 写入审核明细
        $record = new CheckLog();
            $record->commentid = $commentid;
            $record->score = $score;
            $record->executor = $executor;
        $record->save();
    }

    public function actionNote()
    {
        // 获取post
        $post = \Yii::$app->request->post();
            $commentid = $post['commentid'];
            $executor = $post['executor'];
            $notes = $post['notes'];

        // 更新审核结果
        $table = CheckStatus::findOne(['commentid' => $commentid]);
            $table->notes = $notes;
            $table->executor = $executor;
            $table->checkDate = date('Ymd');
        $table->save();

        // 写入审核明细
        $record = new CheckLog();
            $record->commentid = $commentid;
            $record->notes = $notes;
            $record->executor = $executor;
        $record->save();
    }
}
