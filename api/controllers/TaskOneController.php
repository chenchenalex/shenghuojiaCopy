<?php

namespace app\controllers;

use app\models\TaskOne;
use app\models\TaskOneLog;
use app\models\CheckStatus;
use app\controllers\PointController;
use app\functions\StringFunctions;

\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

class TaskOneController extends \yii\web\Controller
{
    public static function happen($id, $month) 
    {
		// 获取当前任务阶段
		$currentStage = self::getCurrentStage($id, $month);

		// 根据当前评价数量，计算应该能到哪个阶段
		$commentCount = self::getCommentCount($id, $month);
		$deservedStage = self::getDeservedStage($commentCount);
    	
    	// 如果 应达到的阶段 > 当前的阶段，则更新任务阶段
    	if ($deservedStage > $currentStage) {
    		self::updateStage($id, $month, $deservedStage);
    	}

    	return [
    		'id' => $id,
    		'month' => $month,
    		'currentStage' => $currentStage,
    		'commentCount' => $commentCount,
    		'deservedStage' => $deservedStage
    	];
    }

    public static function getCommentCount($id, $month)
    {
    	$month = StringFunctions::formatMonth($month, 'm');

        $sql = "SELECT 	id, 
        				substr(commentDate, 5, 2) as month, 
        				count(commentid) as commentCount
                FROM 	shj_checkStatus
                WHERE 	substr(commentDate, 5, 2) = ".$month."
                AND 	checkStatus = 1
                AND		id = ".$id;

        $connection = \Yii::$app->db;
        $results = $connection->createCommand($sql)->queryOne();

        return $commentCount = $results['commentCount'];
    }

    public static function getCurrentStage($id, $month) 
    {
        $result = TaskOne::findOne([
    		'id' => $id,
    		'month' => $month
    	]);

        // 如果存在该用户当月stage记录，则返回
        // 如果不存在，则创建，并返回0
        if ($result) {
            return $stage = $result['stage'];
        } 
        else {
            $record = new TaskOne();
                $record->id = $id;
                $record->month = $month;
                $record->stage = 0;
            $record->save();

            return 0;
        }
    }

    public static function getDeservedStage($commentCount) 
    {
    	switch (1) 
    	{
            case $commentCount >= 18:
                $stage = 6;
                break;

            case $commentCount >= 15:
                $stage = 5;
                break;

            case $commentCount >= 12:
                $stage = 4;
                break;

    		case $commentCount >= 9:
    			$stage = 3;
    			break;

    		case $commentCount >= 6:
    			$stage = 2;
    			break;

    		case $commentCount >= 3:
    			$stage = 1;
    			break;

    		default:
    			$stage = 0;
    			break;
    	}

    	return $stage;
    }

    public static function updateStage($id, $month, $deservedStage) 
    {
    	// 写入阶段表
    	$result = TaskOne::findOne([
    		'id' => $id,
    		'month' => $month
    	]);
    	$result->stage = $deservedStage;
    	$result->save();

    	// 写入阶段日志表
    	$record = new TaskOneLog();
            $record->id = $id;
            $record->month = $month;
            $record->stage = $deservedStage;
            $record->executor = 'System';
        $record->save();

    	// 根据任务阶段，发放积分
		$pointDelta = self::getDeservedPoint($deservedStage);

		if ($pointDelta != 0) 
		{
			$notes = '完成'.$month.'月阶段'.$deservedStage;
			$executor = 'System';
			PointController::update($id, $pointDelta, $notes, $executor);
		}
    }

    public static function getDeservedPoint($stage) 
    {
    	switch ($stage) 
    	{
    		case 1:
    			$deservedPoint = 400;
    			break;

			case 2:
    			$deservedPoint = 400;
    			break;

			case 3:
    			$deservedPoint = 400;
    			break;

    		case 4:
                $deservedPoint = 400;
                break;

            case 5:
                $deservedPoint = 400;
                break;

            case 6:
                $deservedPoint = 400;
                break;
            
    		default:
    			$deservedPoint = 0;
    			break;
    	}

    	return $deservedPoint;
    }
}
