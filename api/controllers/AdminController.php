<?php

namespace app\controllers;

use app\models\Admin;
use app\functions\CookieFunctions;

\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

class AdminController extends \yii\web\Controller
{
    /* 登录 */
    public function actionLogin() {
        $request = \Yii::$app->request;
        
        if ($request->isGet) {
            if (self::hasCookies()) {
                $cookies = \Yii::$app->request->cookies;
                return self::verifyPass($cookies['id'], $cookies['pass']);
            } 
            else {
                return self::denyLogin();
            }
        }

        if ($request->isPost) {
            $post = $request->post();
            return self::verifyPass($post['id'], $post['pass']);
        }
    }

    /* 登出 */
    public function actionLogout(){
        $cookies = \Yii::$app->response->cookies;
        $cookies->remove('id');
        $cookies->remove('pass');
    }

    /* 修改密码 */
    public function actionResetPass() {
        $post = \Yii::$app->request->post();
            $adminid = $post['adminid'];
            $oldPass = $post['oldPass'];
            $newPass = $post['newPass'];

        if (self::passIsCorrect($adminid, $oldPass)) {
            self::setNewPass($adminid, $newPass);
            return ['status' => true];
        } 
        else {
            return ['status' => false];
        }
    }


    /* 
        Functions 
    */

    public static function acceptLogin($id) {
        $user = self::getUserInfo($id);

        return [
            'status' => [
                'code' => 1
            ],
            'admin' => [
                'id' => $user->id,
                'name' => $user->name,
                'avatorSrc' => $user->avatorSrc
            ]
        ];
    }

    public static function getUserInfo($id) {
        return $adminInfo = Admin::findOne($id);
    }

    /* 验证密码 */
    public static function verifyPass($id, $pass) {
        if (self::passIsCorrect($id, $pass)) {
            self::saveLoginStatus($id, $pass);
            return self::acceptLogin($id);
        } 
        else {
            return self::denyLogin();
        }
    }


    /* 登录失败 */
    public static function denyLogin($errorMsg = '') {
        return [
            'status' => [
                'code' => 0,
                'text' => $errorMsg
            ],
            'admin' => []
        ];
    }

    /* 检查是否已有cookies */
    public static function hasCookies() {
        $cookies = \Yii::$app->request->cookies;
        return $cookies->has('id') && $cookies->has('pass');
    }

    /* 获取指定用户全部信息 */
    public static function getAdminInfo($id) {
        return Admin::findOne($id);
    }

    /* 保存cookies */
    public static function saveLoginStatus($id, $pass) {
        if (!self::hasCookies()) {
            CookieFunctions::setCookie('id', $id);
            CookieFunctions::setCookie('pass', $pass);
        }
    }

    /* 清除cookies */
    public static function clearLoginStatus() {
        $cookies = \Yii::$app->response->cookies;
        $cookies->remove('id');
        $cookies->remove('pass');
    }

    /* 验证密码 */
    public static function passIsCorrect($id, $pass) {
        $passInput = hash('md5', $pass);
        $passCorrect = self::getUserInfo($id)->pass;

        return $passInput == $passCorrect;
    }

    /* 设置新密码 */
    public static function setNewPass($adminid, $newPass) {
        $admin = Admin::findOne($adminid);
        $admin->pass = hash('md5', $newPass);
        $admin->save();

        self::clearLoginStatus();
    }
}
