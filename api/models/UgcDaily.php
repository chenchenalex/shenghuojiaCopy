<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ugc_daily".
 *
 * @property integer $date
 * @property integer $countOrder
 * @property integer $countComment
 * @property integer $countCommentStarred
 * @property double $ratioCommented
 * @property double $ratioCommentedIn1day
 * @property double $ratioCommentedIn3days
 * @property double $ratioCommentedIn7days
 * @property double $ratioCommentedIn30days
 * @property double $ratioCommentedAfter30days
 * @property double $ratioCommentedAt28day
 * @property double $ratioCommentLongerThan15words
 * @property double $ratioCommentHasPic
 * @property double $avgCountCommentPic
 * @property double $ratioCommentEnterStarCheck
 * @property double $ratioCommentPassStarCheck
 * @property double $ratioCommentStarred
 */
class UgcDaily extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ugc_daily';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'countOrder', 'countComment', 'countCommentStarred', 'ratioCommented', 'ratioCommentedIn1day', 'ratioCommentedIn3days', 'ratioCommentedIn7days', 'ratioCommentedIn30days', 'ratioCommentedAfter30days', 'ratioCommentedAt28day', 'ratioCommentLongerThan15words', 'ratioCommentHasPic', 'ratioCommentEnterStarCheck', 'ratioCommentPassStarCheck', 'ratioCommentStarred'], 'required'],
            [['date', 'countOrder', 'countComment', 'countCommentStarred'], 'integer'],
            [['ratioCommented', 'ratioCommentedIn1day', 'ratioCommentedIn3days', 'ratioCommentedIn7days', 'ratioCommentedIn30days', 'ratioCommentedAfter30days', 'ratioCommentedAt28day', 'ratioCommentLongerThan15words', 'ratioCommentHasPic', 'avgCountCommentPic', 'ratioCommentEnterStarCheck', 'ratioCommentPassStarCheck', 'ratioCommentStarred'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date' => 'Date',
            'countOrder' => 'Count Order',
            'countComment' => 'Count Comment',
            'countCommentStarred' => 'Count Comment Starred',
            'ratioCommented' => 'Ratio Commented',
            'ratioCommentedIn1day' => 'Ratio Commented In1day',
            'ratioCommentedIn3days' => 'Ratio Commented In3days',
            'ratioCommentedIn7days' => 'Ratio Commented In7days',
            'ratioCommentedIn30days' => 'Ratio Commented In30days',
            'ratioCommentedAfter30days' => 'Ratio Commented After30days',
            'ratioCommentedAt28day' => 'Ratio Commented At28day',
            'ratioCommentLongerThan15words' => 'Ratio Comment Longer Than15words',
            'ratioCommentHasPic' => 'Ratio Comment Has Pic',
            'avgCountCommentPic' => 'Avg Count Comment Pic',
            'ratioCommentEnterStarCheck' => 'Ratio Comment Enter Star Check',
            'ratioCommentPassStarCheck' => 'Ratio Comment Pass Star Check',
            'ratioCommentStarred' => 'Ratio Comment Starred',
        ];
    }
}
