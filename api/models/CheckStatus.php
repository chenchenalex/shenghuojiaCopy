<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shj_checkStatus".
 *
 * @property integer $id
 * @property integer $commentid
 * @property integer $orderid
 * @property string $commentDate
 * @property string $shopName
 * @property string $commentContent
 * @property integer $wordCount
 * @property integer $imageCount
 * @property integer $checkStatus
 * @property integer $score
 * @property string $notes
 * @property string $executor
 * @property string $checkDate
 * @property string $importDate
 * @property string $source
 */
class CheckStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shj_checkStatus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'commentid', 'orderid', 'wordCount', 'imageCount', 'checkStatus', 'score'], 'integer'],
            [['commentDate', 'checkDate', 'importDate'], 'string', 'max' => 8],
            [['shopName', 'commentContent', 'notes'], 'string', 'max' => 255],
            [['executor'], 'string', 'max' => 100],
            [['source'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'commentid' => 'Commentid',
            'orderid' => 'Orderid',
            'commentDate' => 'Comment Date',
            'shopName' => 'Shop Name',
            'commentContent' => 'Comment Content',
            'wordCount' => 'Word Count',
            'imageCount' => 'Image Count',
            'checkStatus' => 'Check Status',
            'score' => 'Score',
            'notes' => 'Notes',
            'executor' => 'Executor',
            'checkDate' => 'Check Date',
            'importDate' => 'Import Date',
            'source' => 'Source',
        ];
    }
}
