<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shj_taskOneLog".
 *
 * @property integer $logid
 * @property integer $id
 * @property integer $month
 * @property integer $stage
 * @property string $executor
 * @property string $createTime
 */
class TaskOneLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shj_taskOneLog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'month', 'stage'], 'required'],
            [['id', 'month', 'stage'], 'integer'],
            [['createTime'], 'safe'],
            [['executor'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'logid' => 'Logid',
            'id' => 'ID',
            'month' => 'Month',
            'stage' => 'Stage',
            'executor' => 'Executor',
            'createTime' => 'Create Time',
        ];
    }
}
