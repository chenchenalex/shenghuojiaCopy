<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shj_point".
 *
 * @property integer $id
 * @property integer $point
 */
class Point extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shj_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'point'], 'required'],
            [['id', 'point'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'point' => 'Point',
        ];
    }
}
