<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shj_taskOne".
 *
 * @property integer $recordid
 * @property integer $id
 * @property integer $month
 * @property integer $stage
 * @property string $modTime
 */
class TaskOne extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shj_taskOne';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'month', 'stage'], 'required'],
            [['id', 'month', 'stage'], 'integer'],
            [['modTime'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'recordid' => 'Recordid',
            'id' => 'ID',
            'month' => 'Month',
            'stage' => 'Stage',
            'modTime' => 'Mod Time',
        ];
    }
}
