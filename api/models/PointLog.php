<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shj_pointLog".
 *
 * @property integer $logid
 * @property integer $id
 * @property integer $delta
 * @property string $executor
 * @property string $notes
 * @property string $createTime
 */
class PointLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shj_pointLog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'delta', 'executor', 'notes'], 'required'],
            [['id', 'delta'], 'integer'],
            [['createTime'], 'safe'],
            [['executor', 'notes'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'logid' => 'Logid',
            'id' => 'ID',
            'delta' => 'Delta',
            'executor' => 'Executor',
            'notes' => 'Notes',
            'createTime' => 'Create Time',
        ];
    }
}
