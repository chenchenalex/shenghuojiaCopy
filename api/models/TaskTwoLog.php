<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shj_taskTwoLog".
 *
 * @property integer $logid
 * @property integer $id
 * @property integer $month
 * @property integer $hasStarredComment
 * @property string $executor
 * @property string $modTime
 */
class TaskTwoLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shj_taskTwoLog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'month', 'hasStarredComment', 'executor'], 'required'],
            [['id', 'month', 'hasStarredComment'], 'integer'],
            [['modTime'], 'safe'],
            [['executor'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'logid' => 'Logid',
            'id' => 'ID',
            'month' => 'Month',
            'hasStarredComment' => 'Has Starred Comment',
            'executor' => 'Executor',
            'modTime' => 'Mod Time',
        ];
    }
}
