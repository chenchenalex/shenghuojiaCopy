<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shj_user".
 *
 * @property integer $id
 * @property integer $vipid
 * @property string $name
 * @property string $tel
 * @property string $city
 * @property string $joinDate
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shj_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'vipid', 'name', 'tel', 'city', 'joinDate'], 'required'],
            [['id', 'vipid'], 'integer'],
            [['name', 'city'], 'string', 'max' => 100],
            [['tel'], 'string', 'max' => 20],
            [['joinDate'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vipid' => 'Vipid',
            'name' => 'Name',
            'tel' => 'Tel',
            'city' => 'City',
            'joinDate' => 'Join Date',
        ];
    }
}
