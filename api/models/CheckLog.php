<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shj_checkLog".
 *
 * @property integer $logid
 * @property integer $commentid
 * @property integer $checkStatus
 * @property integer $score
 * @property string $notes
 * @property string $executor
 * @property string $createTime
 */
class CheckLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shj_checkLog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['commentid', 'executor'], 'required'],
            [['commentid', 'checkStatus', 'score'], 'integer'],
            [['createTime'], 'safe'],
            [['notes', 'executor'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'logid' => 'Logid',
            'commentid' => 'Commentid',
            'checkStatus' => 'Check Status',
            'score' => 'Score',
            'notes' => 'Notes',
            'executor' => 'Executor',
            'createTime' => 'Create Time',
        ];
    }
}
