<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shj_taskTwo".
 *
 * @property integer $recordid
 * @property integer $id
 * @property integer $month
 * @property integer $hasStarredComment
 * @property string $modTime
 */
class TaskTwo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shj_taskTwo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['id', 'month', 'hasStarredComment'], 'required'],
            [['id', 'month', 'hasStarredComment'], 'integer'],
            [['modTime'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'recordid' => 'Recordid',
            'id' => 'ID',
            'month' => 'Month',
            'hasStarredComment' => 'Has Starred Comment',
            'modTime' => 'Mod Time',
        ];
    }
}
